# List of Links to Videos Referenced at Post PyCon Talks 2015

Bloom Filters

* Curtis Lassam - Hash Functions and You: Partners in Freedom [https://www.youtube.com/watch?v=IGwNQfjLTp0
](https://www.youtube.com/watch?v=IGwNQfjLTp0)
* Alex Gaynor - Fast Python, Slow Python
  [https://www.youtube.com/watch?v=7eeEf_rAJds](https://www.youtube.com/watch?v=7eeEf_rAJds)

Pilots and Developers

* Andrew Godwin - What can programmers learn from pilots? [https://www.youtube.com/watch?v=we4G_X91e5w](https://www.youtube.com/watch?v=we4G_X91e5w)

Reusable Code and Beyond PEP 8

* Greg Ward - How to Write Reusable Code
  [https://www.youtube.com/watch?v=r9cnHO15YgU]
(https://www.youtube.com/watch?v=r9cnHO15YgU)
* Raymond Hettinger - Beyond PEP 8 -- Best Practices for Beautiful Code
[https://www.youtube.com/watch?v=wf-BqAjZb8M](https://www.youtube.com/watch?v=wf-BqAjZb8M)
* Raymond Hettinger - Transforming Code into Beautiful, Idiomatic Python
[https://www.youtube.com/watch?v=OSGv2VnC0go](https://www.youtube.com/watch?v=OSGv2VnC0go)

Super considered Super

* Raymond Hettinger - Super considered Super
  [https://www.youtube.com/watch?v=EiOglTERPEo](https://www.youtube.com/watch?v=EiOglTERPEo)
* Raymond Hettinger - Super considered Super Blog Post
  [https://rhettinger.wordpress.com/2011/05/26/super-considered-super/](https://rhettinger.wordpress.com/2011/05/26/super-considered-super/)
